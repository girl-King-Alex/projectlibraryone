﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CharacterCustomKeyboard
{
    class Database
    {
        public IEnumerable<keyboardKeys> keyboardKeys()
        {
            return new List<keyboardKeys>
            {                
                new keyboardKeys{Id = 1, Character=')' },
                new keyboardKeys{Id = 2, Character='(' },
                new keyboardKeys{Id = 3, Character='*' },
                new keyboardKeys{Id = 4, Character='&' },
                new keyboardKeys{Id = 5, Character='^' },
                new keyboardKeys{Id = 6, Character='%' },
                new keyboardKeys{Id = 7, Character='$' },
                new keyboardKeys{Id = 8, Character='#' },
                new keyboardKeys{Id = 9, Character='@' },
                new keyboardKeys{Id = 0, Character='!' },
            };
        }

        public IEnumerable<KeyboardOutputs> KeyboardOutputs()
        {
            return new List<KeyboardOutputs>
            {
                new KeyboardOutputs{Id = 1, Number="1" },
                new KeyboardOutputs{Id = 2, Number="2" },
                new KeyboardOutputs{Id = 3, Number="3" },
                new KeyboardOutputs{Id = 4, Number="4" },
                new KeyboardOutputs{Id = 5, Number="5" },
                new KeyboardOutputs{Id = 6, Number="6" },
                new KeyboardOutputs{Id = 7, Number="7" },
                new KeyboardOutputs{Id = 8, Number="8" },
                new KeyboardOutputs{Id = 9, Number="9" },
                new KeyboardOutputs{Id = 0, Number="0" },
            };
        }
    }
}
