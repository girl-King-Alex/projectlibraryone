﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CharacterCustomKeyboard
{
    class CustomKeyboard
    {
        public static Database database = new Database();
        static bool StringValidator(string userInput)
        {
            var isValidated = false;
            var chars = new List<char> {')', '!', '@', '#', '$', '%', '^', '&', '*', '('};
            foreach (var character in userInput)
            {
                foreach (var item in chars)
                {
                    if (character == item)
                    {
                        isValidated = true;
                        goto OuterLoop;
                    }
                }

                isValidated = false;
                break;

            OuterLoop:
                continue;
            }
            return isValidated;
        }

        public static void InputConverter(string UserInput)
        {
            var isInputValidated = StringValidator(UserInput);
            if (isInputValidated)
            {
                Console.WriteLine();
                foreach (var character in UserInput)
                {
                    var carID = from key in database.keyboardKeys()
                                where key.Character.Equals(character)
                                join output in database.KeyboardOutputs() on key.Id equals output.Id
                                select output.Number;
                    foreach (var x in carID)
                    {
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        Console.Write(x);                       
                    }
                    Console.ForegroundColor = ConsoleColor.White;
                }
                Console.WriteLine();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine();
                Console.WriteLine("Your input is Invalid");               
                Console.ForegroundColor = ConsoleColor.White;                
            }
        }
    }
}
