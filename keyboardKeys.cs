﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CharacterCustomKeyboard
{
    public class keyboardKeys
    {
        public int Id { get; set; }
        public char Character { get; set; }
        public string Number { get; set; }
    }
}
