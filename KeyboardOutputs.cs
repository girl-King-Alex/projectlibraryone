﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CharacterCustomKeyboard
{
    class KeyboardOutputs
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string NumberInWords { get; set; }
    }
}
